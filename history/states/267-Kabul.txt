
state={
	id=267
	name="STATE_267"
	resources={
		tungsten=3.000
	}

	impassable = yes
	history={
		owner = WAS
		add_core_of = WAS
		victory_points = {
			10737 10 
		}
		victory_points = {
			14913 1 
		}
		victory_points = {
			14912 1 
		}
		buildings = {
			infrastructure = 1
			arms_factory = 1
			industrial_complex = 2

		}

	}

	provinces={
		4558 10737 10826 12763 14912 14913 14915 
	}
	manpower=4637140
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
