
state={
	id=657
	name="STATE_657"
	resources={
		steel=18.000
	}

	history={
		victory_points = {
			7672 1 
		}
		buildings = {
			infrastructure = 1
			air_base = 2

		}

	}

	provinces={
		675 4246 4805 7672 10722 12192 12261 12678 14420 
	}
	manpower=308380
	buildings_max_level_factor=1.000
	state_category=pastoral
	local_supplies=0.000
}
